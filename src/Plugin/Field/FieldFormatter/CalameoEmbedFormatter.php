<?php

namespace Drupal\media_entity_calameo\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\media_entity_calameo\Plugin\media\Source\Calameo;

/**
 * Plugin implementation of the 'calameo_embed' formatter.
 *
 * @FieldFormatter(
 *   id = "calameo_embed",
 *   label = @Translation("Calameo embed"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class CalameoEmbedFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var \Drupal\media\MediaInterface $media */
    $media = $items->getEntity();

    $element = [];
    if (($source = $media->getSource()) && $source instanceof Calameo) {
      foreach ($items as $delta => $item) {
        $source_value = $item->value;

        if (!$source_value) {
          break;
        }

        if (stripos($source_value, 'http') !== FALSE) {
          $iframe_url = Url::fromUri($source_value);
        }
        else {
          $calameo_url = "//v.calameo.com/";
          $iframe_url = Url::fromUri($calameo_url, ['query' => ['bkcode' => $source_value]]);
        }

        $element[$delta] = [
          '#theme' => 'media_calameo',
          '#url' => $iframe_url->toString(),
          '#width' => $this->getSetting('width'),
          '#height' => $this->getSetting('height'),
        ];
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getTargetEntityTypeId() === 'media';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'width' => '480',
      'height' => '640',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $this->getSetting('width'),
      '#min' => 1,
      '#description' => $this->t('Width of Calameo.'),
    ];

    $elements['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $this->getSetting('height'),
      '#min' => 1,
      '#description' => $this->t('Height of Calameo.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [
      $this->t('Width: @width px', [
        '@width' => $this->getSetting('width'),
      ]),
      $this->t('Height: @height px', [
        '@height' => $this->getSetting('height'),
      ]),
    ];
  }

}
