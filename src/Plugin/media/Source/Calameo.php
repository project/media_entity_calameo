<?php

namespace Drupal\media_entity_calameo\Plugin\media\Source;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;
use Drupal\media_entity_calameo\CalameoManager;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides media type plugin for Calameo.
 *
 * @MediaSource(
 *   id = "calameo",
 *   label = @Translation("Calameo"),
 *   description = @Translation("Provides business logic and metadata for Calameo."),
 *   allowed_field_types = {
 *     "string"
 *   },
 *   default_thumbnail_filename = "calameo.png",
 *   forms = {
 *     "media_library_add" = "\Drupal\media_entity_calameo\Form\CalameoCreateForm",
 *   }
 * )
 */
class Calameo extends MediaSourceBase {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Calameo manager.
   *
   * @var \Drupal\media_entity_calameo\CalameoManager
   */
  protected $calameoManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;


  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   * @param \Drupal\media_entity_calameo\CalameoManager $calameoManager
   *   Calameo manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    FieldTypePluginManagerInterface $field_type_manager,
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $loggerFactory,
    CalameoManager $calameoManager,
    FileSystemInterface $file_system,
    ClientInterface $http_client,
    DateFormatterInterface $date_formatter
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $entity_field_manager,
      $field_type_manager,
      $config_factory
    );
    $this->loggerFactory = $loggerFactory;
    $this->calameoManager = $calameoManager;
    $this->fileSystem = $file_system;
    $this->httpClient = $http_client;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('media_entity_calameo.manager'),
      $container->get('file_system'),
      $container->get('http_client'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      'ID' => $this->t('ID of the publication'),
      'SubscriptionID' => $this->t('Subscriber\'s folder ID'),
      'AccountID' => $this->t('Subscriber\'s owner account ID (should be your account ID)'),
      'Name' => $this->t('Title of the publication'),
      'Description' => $this->t('Description of the publication'),
      'Category' => $this->t('Category of the publication'),
      'Format' => $this->t('Format'),
      'Dialect' => $this->t('Dialect'),
      'Status' => $this->t('Status'),
      'IsPublished' => $this->t('Status of publication'),
      'IsPrivate' => $this->t('Private content'),
      'AuthID' => $this->t('AuthID'),
      'AllowMini' => $this->t('AllowMini'),
      'Date' => $this->t('Published Date'),
      'Pages' => $this->t('Pages'),
      'Width' => $this->t('Width'),
      'Height' => $this->t('Height'),
      'Views' => $this->t('Views'),
      'Downloads' => $this->t('Downloads'),
      'Comments' => $this->t('Comments'),
      'Favorites' => $this->t('Favorites'),
      'PosterUrl' => $this->t('Poster url'),
      'PictureUrl' => $this->t('Absolute URL for the publication\'s cover'),
      'ThumbUrl' => $this->t('Absolute URL for the publication\'s thumbnail'),
      'PublicUrl' => $this->t('Absolute URL for the publication\'s reading page'),
      'ViewUrl' => $this->t('View url'),
      'Creation' => $this->t('Date of creation of the publication'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $source = $this->getCalameoSource($media);
    if ($source === FALSE) {
      return NULL;
    }

    $publication_id = $source;

    // If source contains an url, we need to extract the shortcode.
    if ($shortcode = $this->matchShortcode($source)) {
      $publication_id = $shortcode;
    }

    if (!$data = $this->calameoManager->getPublicationInfos($publication_id)) {
      return NULL;
    }

    if (!empty($data['response']['error'])) {
      $error_message = $data['response']['error']['message'];
      $this->loggerFactory->get('media_entity_calameo')->error($data['response']['error']['message']);
      $message = $this->t("Calameo media information could not be retrieved: @error", ['@error' => $error_message]);
      $this->messenger()->addError($message);
    }

    $publication_information = $data['response']['content'] ?? NULL;

    switch ($attribute_name) {
      // This is used to set the name of the media entity if the user leaves the field blank.
      case 'default_name':
        return $this->getMetadata($media, 'Name');

      // This is used to generate the thumbnail field.
      case 'thumbnail_uri':
        return isset($publication_information['PosterUrl']) ?
          $this->getLocalThumbnailUri($publication_information['PosterUrl']) : parent::getMetadata($media, 'PosterUrl');

      default:
        return $publication_information[$attribute_name] ?? parent::getMetadata($media, $attribute_name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    return parent::createSourceField($type)->set('label', 'Calameo ID or Calameo URL');
  }

  /**
   * To know if API settings are filling.
   *
   * @return bool
   *   True if settings are filling or false if not.
   */
  protected function getApiSettings() {
    /** @var \Drupal\Core\Config\ImmutableConfig $settings */
    $settings = $this->configFactory->get('media_entity_calameo.settings');

    if ($settings->get('api_key') && $settings->get('api_secret')) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Copy remote thumbnail.
   *
   * @param $remote_thumbnail_url
   *   Remote thumbnail url.
   *
   * @return string|null
   */
  protected function getLocalThumbnailUri($remote_thumbnail_url) {
    if (!$remote_thumbnail_url) {
      return NULL;
    }
    $date = new DrupalDateTime();

    $year = $date->format('Y');
    $month = $date->format('m');

    $directory = $this->configFactory->get('media_entity_calameo.settings')->get('local_images') . "/{$year}/{$month}/";
    $local_thumbnail_uri = "{$directory}/" . Crypt::hashBase64($remote_thumbnail_url) . '.' . pathinfo($remote_thumbnail_url, PATHINFO_EXTENSION);

    if (file_exists($local_thumbnail_uri)) {
      return $local_thumbnail_uri;
    }

    if (!$this->fileSystem
      ->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      $this->loggerFactory
        ->get('media_entity_calameo')->warning('Could not prepare thumbnail destination directory @dir for Calameo media.', [
          '@dir' => $directory,
        ]);
      return NULL;
    }

    try {
      $response = $this->httpClient
        ->get($remote_thumbnail_url);
      if ($response
        ->getStatusCode() === 200) {
        $this->fileSystem
          ->saveData((string) $response
            ->getBody(), $local_thumbnail_uri, FileSystemInterface::EXISTS_REPLACE);
        return $local_thumbnail_uri;
      }
    }
    catch (RequestException $e) {
      $this->loggerFactory->get('media_entity_calameo')
        ->warning($e
          ->getMessage());
    }
    catch (FileException $e) {
      $this->loggerFactory->get('media_entity_calameo')
        ->warning('Could not download remote thumbnail from {url}.', [
          'url' => $remote_thumbnail_url,
        ]);
    }
    return NULL;

  }

  /**
   * Get shortcode from url using regex.
   *
   * @param string
   *   Calameo Url.
   *
   * @return string|null
   *   The shortcode.
   *
   * @see preg_match()
   */
  protected function matchShortcode($source_url) {
    $matches = [];

    if (preg_match('@((http|https):){0,1}//(.+\.){0,1}calameo\.com/read/(?<shortcode>[a-z0-9_-]+)@i', $source_url, $matches)) {
      return $matches['shortcode'];
    }

    return NULL;
  }

  /**
   * Get source field value.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media object.
   *
   * @return string|false
   *   The calameo url or Id or FALSE if there is no field or it contains invalid
   *   data.
   */
  protected function getCalameoSource(MediaInterface $media) {
    if (isset($this->configuration['source_field'])) {
      $source_field = $this->configuration['source_field'];
      if ($media->hasField($source_field)) {
        $property_name = $media->{$source_field}->first()->mainPropertyName();
        $source = $media->{$source_field}->{$property_name};
        return $source;
      }
    }
    return FALSE;
  }

}
