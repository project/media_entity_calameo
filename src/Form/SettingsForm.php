<?php

namespace Drupal\media_entity_calameo\Form;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;

/**
 * Calameo administration config form.
 */
class SettingsForm extends ConfigFormBase {

  use LoggerChannelTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_entity_calameo_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'media_entity_calameo.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $field_type = NULL) {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->config('media_entity_calameo.settings');

    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('Calameo API credentials'),
      '#description' => $this->t('The API Key and API Secret Key can be requested at the <a href=":url" target="_blank">Developers & API page</a>.', [':url' => 'https://developer.calameo.com/content/api']),
      '#open' => TRUE,
    ];

    $form['api']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key.'),
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('Set this to the API Key that Calameo has provided for you.'),
      '#required' => TRUE,
    ];

    $form['api']['api_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Secret Key.'),
      '#default_value' => $config->get('api_secret'),
      '#description' => $this->t('Set this to the API Secret Key that Calameo has provided for you.'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!isset($values['api_key'])) {
      $form_state->setErrorByName(
        'api_key',
        $this->t("Api key is required.'")
      );
    }

    if (!isset($values['api_secret'])) {
      $form_state->setErrorByName(
        'api_key',
        $this->t("Api secret is required.'")
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->config('media_entity_calameo.settings');
    $keys = [
      'api_key',
      'api_secret',
    ];

    foreach ($keys as $key) {
      $config->set($key, $form_state->getValue($key));
    }

    try {
      $config->save();
      $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
    }
    catch (EntityStorageException $e) {
      $this->messenger()
        ->addError($this->t("There is an error during the configuration saving."));
      $this->loggerFactory->get('media_entity_calameo')->error(
        $this->t("Saving the configuration has failed :",
          ['@message' => $e->getMessage()]));
    }
  }

}
