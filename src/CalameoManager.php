<?php

namespace Drupal\media_entity_calameo;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Utility\Error;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Get Calameo's publication information.
 */
class CalameoManager implements CalameoManagerInterface {

  const CALAMEO_API = 'http://api.calameo.com/1.0';

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Calameo manager constructor.
   *
   * @param \GuzzleHttp\Client $client
   *   A HTTP Client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   A logger factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   */
  public function __construct(Client $client, LoggerChannelFactoryInterface $loggerFactory, ConfigFactoryInterface $config_factory) {
    $this->httpClient = $client;
    $this->loggerFactory = $loggerFactory;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getPublicationInfos($id) {
    /** @var \Drupal\Core\Config\ImmutableConfig $settings */
    $settings = $this->configFactory->get('media_entity_calameo.settings');

    if (!$settings->get('api_key') && !$settings->get('api_secret')) {
      $this->loggerFactory->get('media_entity_calameo')->error("Api key or Api secret is missing");
      return FALSE;
    }

    $api_key = $settings->get('api_key');
    $secret_key = $settings->get('api_secret');

    $options = [
      'apikey' => $api_key,
      'action' => 'API.getBookInfos',
      'book_id' => $id,
      'output' => 'JSON',
    ];

    $signature = $secret_key;
    $signature .= 'actionAPI.getBookInfos';
    $signature .= "apikey{$api_key}";
    $signature .= "book_id{$id}";
    $signature .= "outputJSON";
    $options['signature'] = md5($signature);

    $queryParameter = UrlHelper::buildQuery($options);

    try {
      $response = $this->httpClient->request(
        'GET',
        self::CALAMEO_API . '?' . $queryParameter,
        ['timeout' => 5]
      );
      if ($response->getStatusCode() === 200) {
        $data = Json::decode($response->getBody()->getContents());
      }
    }
    catch (RequestException $e) {
      $this->loggerFactory->get('media_entity_calameo')->error("Could not retrieve Calameo publication information: $id.", Error::decodeException($e));
    }

    if (isset($data)) {
      return $data;
    }

    return FALSE;
  }

}
