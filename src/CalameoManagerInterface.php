<?php

namespace Drupal\media_entity_calameo;

/**
 * Defines a wrapper for Calameo api call.
 */
interface CalameoManagerInterface {

  /**
   * Retrieves publication information.
   *
   * @param int $id
   *   The publication shortcode.
   *
   * @return array
   *   An array containing Calameo's publication information.
   */
  public function getPublicationInfos($id);

}
