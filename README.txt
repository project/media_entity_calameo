Media Entity Calameo
==============================

Description
===========
Provides integration of Calameo publications with the Media Entity module.

Installation
============

This module requires the core Media module.

1. Enable first the core Media and Media Entity Calameo module.
2. Go to /admin/config/media/media_entity_calameo and add your API Key and Api Secret Key of Calameo.
3. Go to /admin/structure/media and click 'Add media bundle' to create a new bundle.
4. In the field Type provider, select Calameo.

Configuration
============

1. Go to Manage display section for the media bundle.
2. For the source field selected, select Calameo embed under Format.
3. Click on the settings icon to configure Calameo's iframe width and height.
4. Save.

Uninstallation
==============
1. Uninstall the module from 'Administer >> Modules'.
